#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void* HEAP_START = (void*)0x40400000;
void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }

static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
	//printf("#block_init\n");
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

// check whether region's address is NULL
extern inline bool region_is_invalid( const struct region* r );

//  Allocating memory using mmap() function.
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */

// Region is allocated as a one big block (I think)
static struct region alloc_region  ( void const * addr, size_t query ) {
    printf("#Allocating region\n");
    size_t size = region_actual_size(query);
    struct region new_region = {0};
    new_region.addr = map_pages(addr, size, MAP_FIXED);
    new_region.size = size;
    if(new_region.addr == MAP_FAILED){
      printf("Failed\n");
      new_region.addr = map_pages(addr, size, 0);
      if(addr == HEAP_START) HEAP_START = new_region.addr;
      if(new_region.addr == MAP_FAILED) return REGION_INVALID;
    }
    block_size b_size;
    b_size.bytes = size;
    block_init(new_region.addr,b_size,NULL);
    return new_region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
	//printf("#heap_init\n");
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
	//printf("#block_splittable\n");
  // Split one big block into two block so that the second block would have at least BLOCK_MIN_CAPACITY length after splitting.
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
	  //printf("#split_if_too_big\n");
    // After checking either or not current block is splittable
    // we are going to split it if possible.
    if (block_splittable(block,query)){
        size_t new_block_size = block->capacity.bytes-query;
        block_capacity b_capacity;
        block_size nb_size;

        void* new_block_next = block->next;
        b_capacity.bytes = query;
        block->capacity = b_capacity;
        block->next = (struct block_header*)block_after(block);
        nb_size.bytes = new_block_size;
        block_init(block->next,nb_size,new_block_next);
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
	  //printf("#block_after\n");
    //Content address + capacity = next block address
    return  (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd ) {
	  //printf("#blocks_continuous\n");
    return (void*)snd == block_after(fst); // does second come after first
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
	  //printf("#mergeable\n");
    // both first and second are free, and come after each other
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd );
}

static bool try_merge_with_next( struct block_header* block ) {
    //printf("#try_merge_with_next\n");
  /*
    1) Get block after block. -> (second)
    2) Check if they come after each other
    3) If yes, then check are they mergeable
    4) If yes then merge first (block) and second (block after block)
  */
    if (block->next == NULL) return false;
    struct block_header* bl_after = (struct block_header*)block_after(block);
    if(mergeable(bl_after,block)){
        block->next = bl_after->next;
        block->capacity.bytes += size_from_capacity(bl_after->capacity).bytes;
        return true;
    }
    
    return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last( struct block_header* block, size_t sz )    {
  //printf("#find_good_or_last\n");
  /*
   1) Search for compatible _block, starting from block
   2) If good block is found.
        2.1) Split it if needed.
        2.2) Set block property of block_search_result to block.
        2.3) Set type property to BSR_FOUND_GOOD_BLOCK.
   3) If iterator reached to the last block, and no good block is found.
        3.1) Set block property of block_search_result to the last block.
        3.2) Set type property to BSR_REACHED_END_NOT_FOUND.
   4) BSR_CORRUPTED ????
  */
  struct block_search_result result = {0};
  struct block_header* temp;
  

  if (block == NULL) result.type = BSR_CORRUPTED;
  else {
    while(block != NULL){
          if(block->is_free){
              if(try_merge_with_next(block)) continue;
              if(block_is_big_enough(sz,block)){
                  split_if_too_big(block,sz);
                  result.block = block;
                  result.type = BSR_FOUND_GOOD_BLOCK;
                  return result;
              }
          }
          temp = block;
          block = block->next;
      }
    result.block = temp;
    result.type = BSR_REACHED_END_NOT_FOUND;
  }
  
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing( size_t query, struct block_header* block ) {
  //printf("#try_memalloc_existing\n");
  struct block_search_result search_result = find_good_or_last(block, query);
  if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(search_result.block, query);
    search_result.block->is_free = false;
  }
  return search_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
		//printf("#grow_heap\n");
    struct region new_region = alloc_region(block_after(last), query);
    last->next = new_region.addr;
    if (try_merge_with_next(last)) return last;
    else return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
	//printf("#memalloc %p\n",heap_start);
	struct block_header* block;
  query = size_max(query,BLOCK_MIN_CAPACITY);
  struct block_search_result result = try_memalloc_existing(query,heap_start);
	if(result.type == BSR_REACHED_END_NOT_FOUND){
		block = try_memalloc_existing(query,grow_heap(result.block, query)).block;
	}
	else if( result.type == BSR_FOUND_GOOD_BLOCK){
		block = result.block;
	}
	else {
  	return NULL;
  }
  return block;
}

void* _malloc( size_t query ) {
	//printf("#_malloc\n");
  //Pass needed size for allocating , and the beginning of heap to start searching from.
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  //Minus the length of the block_header from the content's pointer in order to get at the beginning of block( block_header)
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
