#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include "tests.h"

static void clean_up(){
    _free((uint8_t*)HEAP_START + offsetof(struct block_header, contents));
}

static void block_check(void* block,const char* block_name){
    if(block == NULL){
        printf("Error: Memory allocation failed %s\n",block_name);
    }else {
        debug_heap(stdout, HEAP_START);
        if(block >= HEAP_START && (uint8_t*)block < (uint8_t*)HEAP_START + offsetof(struct block_header, contents)){
            printf("Error: Allocated memory is outside heap range %s\n",block_name);
        }else {
            _free(block);
            clean_up();
            printf("\n");
        }
    }
}


static void normal_allocation_test1() {
    printf("normal_allocation_test1:\n");
    debug_heap(stdout, HEAP_START);
    void *block1 = _malloc(SM_BLOCK);
    block_check(block1,"block 1/1");
    _free(block1);
    clean_up();
    printf("\n");

    
}

static void free_one_block_test2() {
    printf("free_one_block_test2:\n");
    void *block1 = _malloc(SM_BLOCK);
    void *block2 = _malloc(BG_BLOCK);
    void *block3 = _malloc(MD_BLOCK);
    debug_heap(stdout, HEAP_START);
    block_check(block1,"block 2/1");
    block_check(block2,"block 2/2");
    block_check(block3,"block 2/3");
    _free(block1);
    debug_heap(stdout, HEAP_START);
    _free(block2);
    _free(block3);
    clean_up();
    printf("\n");
}

static void free_multiple_blocks_test3() {
    printf("free_multiple_blocks_test3:\n");
    void *block1 = _malloc(SM_BLOCK);
    void *block2 = _malloc(MD_BLOCK);
    void *block3 = _malloc(MD_BLOCK);
    debug_heap(stdout, HEAP_START);
    block_check(block1,"block 3/1");
    block_check(block2,"block 3/2");
    block_check(block3,"block 3/3");
    _free(block1);
    _free(block3);
    debug_heap(stdout, HEAP_START);
    _free(block2);
    debug_heap(stdout, HEAP_START);
    clean_up();
    printf("\n");
}

static void new_region_extends_old_test4() {
    printf("new_region_extends_old_test4:\n");
    debug_heap(stdout, HEAP_START);
    void *block = _malloc(BG_BLOCK);
    block_check(block,"block 4/1");
    _free(block);
    debug_heap(stdout, HEAP_START);
    clean_up();
    printf("\n");
}

static void new_region_with_larger_size_test5() {
    printf("new_region_with_larger_size_test5:\n");
    debug_heap(stdout, HEAP_START);
    void *block = _malloc(GNT_BLOCK);
    block_check(block,"block 5/1");
    _free(block);
    debug_heap(stdout, HEAP_START);
    clean_up();
    printf("\n");
}

void run_tests(){
    normal_allocation_test1();
    free_one_block_test2();
    free_multiple_blocks_test3();
    new_region_extends_old_test4();
    new_region_with_larger_size_test5();
}
