#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include "tests.h"

//extern void debug_heap( FILE* f,  void const* ptr );

int main(){
    heap_init(1);
    run_tests();
    return 0;
}
