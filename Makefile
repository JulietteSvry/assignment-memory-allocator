# # compiler:
# CC = gcc

# # compiler flags: 
# CFLAGS  = --std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG

# # build directory
# BUILDDIR=build

# # the build target executable:
# TARGET = executable

# # object files to build:
# OBJ = $(BUILDDIR)/main.o $(BUILDDIR)/mem.o $(BUILDDIR)/util.o

# all: $(TARGET)

# build:
# 	mkdir $(BUILDDIR)

# $(TARGET): $(OBJ)
# 	$(CC) $(CFLAGS) -o $(TARGET) $(OBJ)

# clean:
# 	rm -rf $(BUILDDIR)
CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=src
CC=gcc

all: $(BUILDDIR)/main.o $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
